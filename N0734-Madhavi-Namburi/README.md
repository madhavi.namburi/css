CSS3 Assignment Questions

Rules:
• All this examples should be in different html files but css file will be only one called as style.css and all css code should be in style file no inline or internal code is allowed.
• After completing assignment you should add your assignment files in one folder and upload/push to the gitlab. Folder name should be Your Employee id and & Name example:
N0605-Mohammed-Ghani

1. Explain what are units in CSS & Explain what is the use of max/min-width, max/min-height with example?
2. What is veiwport & vMax, vMin in css, give one example?
3. Create a div with red and yellow gradient background with four side borders and every side border style should be different.
4. Make 4 Div's with different backgrounds and arrange them with z-index.
5. Make a Custom Style list using fontawesome icon.
6. Make a simple login form with colourful background image and it should be aligned perfect centre without enabling scroll bar.
7. Explain about flex and Give two different examples of flex?
8. Explain about grid and Give two different examples of grid?
9. Explain what is media queries and their properties and make a simple navigation bar should be inline in desktop and stacked in mobile?
10. Create a simple animation to bounce a ball up and down 3 times.